import Observable from './observer.ts'

export class Input {
  constructor (...params: any[]) {
    //
  }
}

export class NumericInput extends Input {
  constructor(id: string) {
    super(id);
    const hostElement: HTMLElement = document.getElementById(id),
      input = document.createElement('input'),
      observer = new Observable();

    if (hostElement) {
      input.addEventListener('input', (e: Event) => {
        const value = (e.target as HTMLInputElement).value;

        if (!Number.isNaN(Number(value))) {
          this.value = Number(value)
        }
        this._text = value
      });
      input.setAttribute('class', 'numeric');
      hostElement.appendChild(input);
      hostElement.setAttribute('class', 'host');
      this._input = input;
      this._isValid = false;
      this._text = null;
      this._value = null;
      this.hostElement = hostElement;
    }
    this.observer = observer
  }
  private _input: HTMLInputElement
  private _isValid: Boolean
  private _text: string
  private _value: number
  public hostElement: HTMLElement
  public observer: Observable
  public get input () {
    return this._input
  }
  public set input (v) {
    //
  }
  public get isValid () {
    return this._isValid
  }
  public set isValid (v) {
    //
  }
  public get text () {
    return this._text
  }
  public set text (str: string) {
    let num = Number(str);

    Number.isFinite(num) || (num = null);
    if (str) {
      this._input.setCustomValidity("");
      this._input.value = str;
      this._isValid = true;
      this._value = num;
      this._text = str
    } else {
      this._input.setCustomValidity("Text is not a valid string");
      this._input.value = "";
      this._isValid = false;
      this._text = null;
      this._value = null
    }
    this.observer.notify(this)
  }
  public get value () {
    return this._value
  }
  public set value (num: number) {
    const str = String(num);

    if (Number.isFinite(num)) {
      this._input.setCustomValidity("");
      this._input.value = str;
      this._isValid = true;
      this._value = num;
      this._text = str
    } else {
      this._input.setCustomValidity("Value is not a valid number");
      this._input.value = "";
      this._isValid = false;
      this._text = null;
      this._value = null
    }
    this.observer.notify(this)
  }
}
