export default class Observable {
  constructor() {
    this.observers = [];
  }
  private observers: Function[]
  public notify(data: object) {
    this.observers.forEach(observer => observer(data));
  }
  public subscribe(f: Function) {
    this.observers.push(f);
  }
  public unsubscribe(f: Function) {
    this.observers = this.observers.filter(subscriber => subscriber !== f);
  }
}
